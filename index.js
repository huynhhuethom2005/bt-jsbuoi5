// Tính kết quả thi
function mathDiem() {
  let khuVuc = document.getElementById("khuVuc").value * 1;
  let doiTuong = document.getElementById("doiTuong").value * 1;
  let diemChuan = document.getElementById("diemChuan").value * 1;
  let mon1 = document.getElementById("mon1").value * 1;
  let mon2 = document.getElementById("mon2").value * 1;
  let mon3 = document.getElementById("mon3").value * 1;
  let resultDom;
  let resultDiemTong = mon1 + mon2 + mon3 + khuVuc + doiTuong;
  if (mon1 == 0 || mon2 == 0 || mon3 == 0) {
    resultDom = `Bạn đã rớt`;
  } else if (resultDiemTong >= diemChuan) {
    resultDom = `Bạn đã đậu với điểm tổng là ${resultDiemTong}`;
  } else if (resultDiemTong < diemChuan) {
    resultDom = `Bạn đã rớt với điểm tổng là ${resultDiemTong}`;
  }
  document.getElementById("resultDiem").innerHTML = resultDom;
}

// Tính tiền điện
function mathKw() {
  const gia50KwDau = 500;
  const gia50KwKe = 650;
  const gia100KwKe = 850;
  const gia150KwKe = 1100;
  const giaKwConlai = 1300;

  let soKw = document.getElementById("soKw").value * 1;
  let resultTien;

  if (soKw < 0) {
    return alert("Số Kw nhập vào không hợp lệ");
  }

  if (soKw < 50) {
    resultTien = soKw * gia50KwDau;
  } else if (soKw <= 100) {
    resultTien = 500 + (soKw - 50) * gia50KwKe;
  } else if (soKw <= 200) {
    resultTien = 500 + 50 * gia50KwKe + (soKw - 100) * gia100KwKe;
  } else if (soKw <= 350) {
    resultTien =
      500 + 50 * gia50KwKe + 100 * gia100KwKe + (soKw - 200) * gia150KwKe;
  } else if (soKw > 350) {
    resultTien =
      500 +
      50 * gia50KwKe +
      100 * gia100KwKe +
      150 * gia150KwKe +
      (soKw - 350) * giaKwConlai;
  }
  document.getElementById(
    "resultKw"
  ).innerHTML = `Số tiền phải trả là ${resultTien}`;
}
